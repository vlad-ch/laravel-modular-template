## Documentation

### Conventions
- [Laravel Best Practices](https://github.com/alexeymezenin/laravel-best-practices)
- [Laravel API Best Practices](https://gist.github.com/fomvasss/792001644b31b64add866bd5a1610a4e)
- [JSON:API](https://jsonapi.org/)

### [Laravel](https://packagist.org/packages/laravel/laravel)

- [Docs](https://laravel.com/docs/)
- [Bootcamp](https://bootcamp.laravel.com/)
- [Laracasts](https://laracasts.com/)
- [GitHub](https://github.com/laravel/laravel)

### [barryvdh/laravel-debugbar](https://packagist.org/packages/barryvdh/laravel-debugbar)

- [GitHub](https://github.com/barryvdh/laravel-debugbar)

### [laravel-shift/blueprint](https://packagist.org/packages/laravel-shift/blueprint)

- [Docs](https://blueprint.laravelshift.com/docs/advanced-configuration/)
- [GitHub](https://github.com/laravel-shift/blueprint)

### [nwidart/laravel-modules](https://packagist.org/packages/nwidart/laravel-modules)

- [Docs](https://docs.laravelmodules.com/)
- [GitHub](https://github.com/nWidart/laravel-modules)

### [prettus/l5-repository](https://packagist.org/packages/prettus/l5-repository)

- [GitHub](https://github.com/andersao/l5-repository)

### [spatie/laravel-backup](https://packagist.org/packages/spatie/laravel-backup)

- [Docs](https://spatie.be/docs/laravel-backup/)
- [GitHub](https://github.com/spatie/laravel-backup)

### [spatie/laravel-data](https://packagist.org/packages/spatie/laravel-data)

- [Docs](https://spatie.be/docs/laravel-data/)
- [GitHub](https://github.com/spatie/laravel-data)

### [spatie/laravel-permission](https://packagist.org/packages/spatie/laravel-permission)

- [Docs](https://spatie.be/docs/laravel-permission/)
- [GitHub](https://github.com/spatie/laravel-permission)

### [spatie/laravel-query-builder](https://packagist.org/packages/spatie/laravel-query-builder)

- [Docs](https://spatie.be/docs/laravel-query-builder/)
- [GitHub](https://github.com/spatie/laravel-query-builder)

